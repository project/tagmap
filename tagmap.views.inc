<?php

/**
 * @file
 *
 */

/**
 * Implementation of hook_views_plugins
 */
function tagmap_views_plugins() {
  $path = drupal_get_path('module', 'tagmap');
  $views_path = drupal_get_path('module', 'views');
  
  return array(    
    'style' => array(
      'tagmap' => array(
      'title' => t('Tagmap'),
      'help' => t('Display terms over google map'),
      'handler' => 'views_plugin_style_tagmap',
      'theme' => 'views_view_tagmap',
      'theme file' => 'tagmap.theme.inc',
      'uses row plugin' => TRUE,
      'uses fields' => FALSE,
      'uses options' => TRUE,
      'type' => 'normal',
      ),
    ),    
  );
}