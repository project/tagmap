<?php

/**
 * @file
 * Install file for the tagmap module.
 *
 */
 
 
/**
 * Implementation of hook_schema().
 */
function tagmap_schema() {
  $schema['tagmap_embeds'] = array(
    'description' => 'Embed visualization files (html) made by tagmap module',
    'fields' => array(
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'File id of the embed file.',
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 60,
        'not null' => FALSE,
        'default' => '',
        'description' => 'Title of the visualization.',
      ),          
      'link_view' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
        'description' => 'Link to the edit form in Views module.',
      ),  
      'link_vidi' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
        'description' => 'Link to the edit form in VIDI module.',
      ),      
      'form_state_storage' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'description' => 'Form_state for VIDI module.',
      ), 
      'visualisation_code' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'description' => 'Iframe for visualization.',
      ),            
    ),
    'primary key' => array('fid'),
  );
  
  $schema['tagmap_preset'] = array(
    'description' => 'The color presets for the tagmap',
    'fields' => array(
      'prid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique ID for table.',
      ),        
      'preset_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
        'description' => 'The name of the color preset.',
      ),              
    ),
    'primary key' => array('prid'),
  );
  
  $schema['tagmap_preset_color'] = array(
    'description' => 'The color presets for the tagmap',
    'fields' => array(
      'colorid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique ID for table.',
      ),    
      'prid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Id of the preset.',
      ),           
      'color_code' => array(
        'type' => 'varchar',
        'length' => 7,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Color hex code.',
      ),  
      'color_index' => array(
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Index of a color - the order is important.',
      ),                  
    ),
    'primary key' => array('colorid'),
  );
  
  $schema['tagmap_layers'] = array(
    'description' => 'The color presets for the tagmap',
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique ID for table.',
      ),    
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The name of the layer/overlay.',
      ),           
      'title' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The title of the layer/overlay.',
      ),  
      'baselayer' => array(
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => 'Is the new layer a baselayer.',
      ),
      'options' => array(
        'type' => 'text',
        'size' => 'normal',
        'not null' => FALSE,
        'description' => 'The serialized options for the layer.',
      ),        
                       
    ),
    'primary key' => array('lid'),
  );      
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function tagmap_install() {
  // Create tables
  drupal_install_schema('tagmap');
}

/**
 * Implementation of hook_uninstall().
 */
function tagmap_uninstall() {
  drupal_uninstall_schema('tagmap');
  variable_del('tagmap_settings');
  variable_del('tagmap_api_key');
}

function tagmap_update_6100() {
  $ret = array();
  if (!db_table_exists('tagmap_embeds')) {
    $schema['tagmap_embeds'] = array(
      'description' => 'Embed visualization files (html) made by tagmap module',
      'fields' => array(
        'fid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'File id of the embed file.',
        ),
        'title' => array(
          'type' => 'varchar',
          'length' => 60,
          'not null' => FALSE,
          'default' => '',
          'description' => 'Title of the visualization.',
        ),          
        'link_view' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
          'description' => 'Link to the edit form in Views module.',
        ),  
        'link_vidi' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
          'description' => 'Link to the edit form in VIDI module.',
        ),      
        'form_state_storage' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
          'description' => 'Form_state for VIDI module.',
        ), 
        'visualisation_code' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
          'description' => 'Iframe for visualization.',
        ),            
      ),
      'primary key' => array('fid'),
    );
    db_create_table($ret, 'tagmap_embeds', $schema['tagmap_embeds']);
  }
  if (!db_table_exists('tagmap_preset')) {
    $schema['tagmap_preset'] = array(
      'description' => 'The color presets for the tagmap',
      'fields' => array(
        'prid' => array(
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Unique ID for table.',
        ),        
        'preset_name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
          'description' => 'The name of the color preset.',
        ),              
      ),
      'primary key' => array('prid'),
    );
    db_create_table($ret, 'tagmap_preset', $schema['tagmap_preset']);
  }
  if (!db_table_exists('tagmap_preset_color')) {
    $schema['tagmap_preset_color'] = array(
      'description' => 'The color presets for the tagmap',
      'fields' => array(
        'colorid' => array(
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Unique ID for table.',
        ),    
        'prid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Id of the preset.',
        ),           
        'color_code' => array(
          'type' => 'varchar',
          'length' => 7,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Color hex code.',
        ),  
        'color_index' => array(
          'type' => 'int',
          'size' => 'normal',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Index of a color - the order is important.',
        ),                  
      ),
      'primary key' => array('colorid'),
    );
    db_create_table($ret, 'tagmap_preset_color', $schema['tagmap_preset_color']);
  } 
  return $ret;
} 

function tagmap_update_6101() {
  $ret = array();
  if (!db_table_exists('tagmap_layers')) {
    $schema['tagmap_layers'] = array(
      'description' => 'The color presets for the tagmap',
      'fields' => array(
        'lid' => array(
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Unique ID for table.',
        ),    
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The name of the layer/overlay.',
        ),           
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The title of the layer/overlay.',
        ),  
        'baselayer' => array(
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 1,
          'description' => 'Is the new layer a baselayer.',
        ),
        'options' => array(
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
          'description' => 'The serialized options for the layer.',
        ),                     
      ),
      'primary key' => array('lid'),
    );  
    db_create_table($ret, 'tagmap_layers', $schema['tagmap_layers']);
  }    
  return $ret;
} 