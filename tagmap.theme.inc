<?php
/**
 * @file
 * Main views plugin, creates Tagmap as an option under the Style options when a view is created.
 * Created for Drupal 6 
 */
function template_preprocess_views_view_tagmap(&$vars) {
  $vars['map_object'] = $vars['rows'];
  // Rows is actually our map object.
  unset($vars['rows']);
  // Include all js and css files. Do not remove this line! 
  tagmap_initialize();
  $vars['output'] = theme('tagmap', array('#settings' => $vars['map_object']));  
}

/**
 * Set up the HTML header for Tagmap.
 * If you are going to include a custom JS file that extends Tagmap, you probabaly
 * want to call this first to ensure that the core js files have been added.
 */
function tagmap_initialize() {
  static $tagmap_initialized = FALSE;
  if ($tagmap_initialized) {
    return;
  } 
  $tagmap_path = drupal_get_path('module', 'tagmap');
  
  drupal_add_css($tagmap_path . '/tagmap.css');  
  drupal_add_js($tagmap_path .'/js/textmarker_marker.js', 'module', 'header', FALSE, TRUE, FALSE);  
  drupal_add_js($tagmap_path .'/js/tagmap.js', 'module', 'header', FALSE, TRUE, FALSE);  

  global $language;
  $query = array(
    'file' => 'api',
    'v' => variable_get('gmap_api_version', TAGMAP_GOOGLE_API_VERSION),
    'key' => variable_get('tagmap_api_key', ''),
    'hl' => $language->language,
  );  
  
  drupal_set_html_head('<script src="'. check_url(url('http://maps.google.com/maps', array('query' => $query))) .'" type="text/javascript"></script>');
  //TODO Add condidtional include of these overlays
  drupal_set_html_head('<script src="'. check_url(url('http://js.mapbox.com/g/2/mapbox.js')) .'" type="text/javascript"></script>');

  $tagmap_initialized = TRUE;
}