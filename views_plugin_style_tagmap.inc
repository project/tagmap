<?php

/**
 *  @file
 *
 * Style plugin to render a tagmap.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_tagmap extends views_plugin_style {
  
/**
 * Set default options
 */
  function option_definition() {
    drupal_add_tabledrag('arrange', 'order', 'sibling', 'weight');
    $options = parent::option_definition();
    $options['steps'] = array('default' => '6');
    return $options;
  }

  function query() {
    $this->real_field = ($this->options['tagfield'] ? $this->options['tagfield'] : 'name');
    $this->field_alias = $this->view->query->add_field($this->view->display[$this->view->current_display]->display_options['fields'][$this->real_field]['table'], $this->real_field);
    $this->view->query->add_orderby($this->view->base_table, $this->real_field, 'ASC');
  }

  function render($values) {
    if (empty($this->row_plugin)) {
      vpr('Missing row plugin');
      return;
    }
    // Determine fieldname for latitude and longitude fields.
    $lat_field = $this->view->display_handler->get_handler('field', $this->options['latfield'])->field_alias;
    $lon_field = $this->view->display_handler->get_handler('field', $this->options['lonfield'])->field_alias;

    $tid_field = 'tid';

    // Determine fieldname for term name field.
    $term_name_field = ($this->view->display_handler->get_handler('field', $this->options['tagfield'])->field_alias ? $this->view->display_handler->get_handler('field', $this->options['tagfield'])->field_alias : 'term_data_name');

    // Determine fieldname for term weight field.
    if ($this->options['weightfield']) {
      $weight_field = $this->view->display_handler->get_handler('field', $this->options['weightfield'])->field_alias;
    }

    // Generate markers from view resultset
    $output = '';
    $markers = array();

    //Sets the dimensions of the image to be rendered. Only if ImageCache module is present. Pixels only.
    $image_field = ''; $imagecache_namespace = ''; $width; $height;
    if (module_exists('imagecache') && $this->options['imagecache_icon'] != '--' && $this->options['imagefield'] != '--') {
      $image_field = $this->view->display_handler->get_handler('field', $this->options['imagefield'])->field_alias;
      $imagecache_namespace = $this->view->style_options['imagecache_icon'];
      $preset_actions = imagecache_preset_actions(imagecache_preset_by_name($imagecache_namespace));
      foreach ($preset_actions as $preset) {
        if (isset($preset['data']['width']) && count(explode($preset['data']['width'], '%')) == 1) {
          if (!isset($width) || ((int)$width) < ((int)$preset['data']['width']))
            $width = $preset['data']['width'];
        }
        if (isset($preset['data']['height']) && count(explode($preset['data']['height'], '%')) == 1) {
          if (!isset($height) || ((int)$height) < ((int)$preset['data']['height']))
            $height = $preset['data']['height'];
        }
      }
      if (isset($width)) $width .= 'px';
      if (isset($height)) $height .= 'px';
    }
    if (count($this->view->result) != 0)
      $this->row_plugin->render($this->view->result);
      
    $this->view->row_index = 0;

    foreach ($this->view->result as $label => $row) {
      //print_r ($row);
      if (module_exists('imagecache') && $this->options['imagecache_icon'] != '--' && $this->options['imagefield'] && $this->options['imagefield'] !='--') {
        $regex  = '/(<a\s*'; // Start of anchor tag
        $regex .= '(.*?)\s*'; // Any attributes or spaces that may or may not exist
        $regex .= 'href=[\'"]+?\s*(?P<link>\S+)\s*[\'"]+?'; // Grab the link
        $regex .= '\s*(.*?)\s*>\s*'; // Any attributes or spaces that may or may not exist before closing tag
        $regex .= '(?P<name>\S+)'; // Grab the name
        $regex .= '\s*<\/a>)/i'; // Any number of spaces between the closing anchor tag (case insensitive)  

        $matches = array();     
        
        if (preg_match($regex, $this->rendered_fields[$this->view->row_index][$this->options['imagefield']], $matches)) {
          $image_full_path = empty($matches['link']) ? FALSE : $matches['link'];
          global $base_url;
          $image_full_path = str_ireplace($base_url . '/', '', $image_full_path);
        }
        else
          $image_full_path = $this->rendered_fields[$this->view->row_index][$this->options['imagefield']];
          
        $image_thumb_path = base_path() . imagecache_create_path($imagecache_namespace, $image_full_path);        
//        $image_full_path = $row->{$image_field};
//        $image_thumb_path = base_path() . imagecache_create_path($imagecache_namespace, $row->{$image_field});

      }
      $lat = (float)$row->{$lat_field};
      $lon = (float)$row->{$lon_field};
      $tid = $row->{$tid_field};
      $term_name = $row->{$term_name_field};

      if (!$weight_field) {
        $weight = tagmap_term_count_nodes($row->{$tid_field});
      }
      else {
        $weight = $row->{$weight_field};
      }
      $info_html = "";

      $sort_array = $this->options['table'];
      uasort($sort_array, '_tagmap_element_sort');
      foreach ($sort_array as $key => $value) {
        if ($value['add'] == 0)
          unset($sort_array[$key]);
      }
      $sort_array = array_keys($sort_array);

      if (count($sort_array) != 0 || isset($this->options['description_title']) || isset($image_thumb_path)) {
        $tmp = array();
        foreach ($sort_array as $value)
          $tmp[$value] = $value;
        $sort_array = $tmp; unset($tmp);        

        $info_html_title .= '<div class="tagmap-view-info">';
        if (!empty($this->options['description_title'])) {
          $title_field = $this->view->display_handler->get_handler('field', $this->options['description_title'])->field_alias;
          $info_html .= '<div class="tagmap-view-info-title">' . $row->{$title_field} . '</div>';
        }
        if (isset($image_thumb_path)) { 
          $style = "";
          if (isset($width))
            $style .= "width:" . $width . "; ";
          if (isset($height))
            $style .= "height:". $height . "; ";
          if (module_exists('lightbox2')) {
            $small_image = '<img src="' . $image_thumb_path . '"></img>';
            $info_html .= '<div style="' . $style . '" class="tagmap-cloud-image">' . l($small_image, $image_full_path, array('attributes' => array('onclick' => 'Lightbox.start(this, false, false, false, false); return false;', 'class' => 'lightbox-processed'), 'html' => TRUE)) . '</div>';
          }
          else{        
            $info_html .= '<div style="' . $style . '" class="tagmap-cloud-image"><img src="' . $image_thumb_path . '"></img></div>';
          }
        }            
        $info_html .= '<div class="tagmap-view-info-content">';

        $class = 0; 
        foreach ($sort_array as $value) {
          $val = $this->view->display_handler->get_handler('field', $value)->field_alias;
          $info_html .= "<div class=\"tagmap-cloud-paragraph paragraph-" . $class . "\">" . $row->{$val} . '</div>';
        }
        $info_html .= '</div>';
        $info_html .= '</div>';
      }
      
      if ($weight || $weight == 0) {
        $markers[] = array(
            'latitude' => $lat,
            'longitude' => $lon,
            'term' => $term_name,
            'weight' => $weight,
            'infoText' => $info_html,
            'minFontSize' => $this->options['fontsize'],
            'fontStyle' => $this->options['fontstyle'],
            'markerColor' => 'css',
        );
      }
      else {
        if (empty($weight)) {
          drupal_set_message(t('The tagmap could not be shown because a weight field is not set.'), 'error');
        }
      }
      $this->view->row_index++;
    }
    tagmap_generate_weighted_markers($markers, $this->view->style_plugin->options['steps'], $this->options['color']);

    // If there are no markers, don't draw empty map.
    if (!empty($markers)) {
      $map['markers'] = $markers;
      $map['embed_link'] = $this->view->style_plugin->options['map_settings']['embed_link'];
      if (isset($this->view->style_plugin->options['map_settings']['width'])) {
        $map['width'] = $this->view->style_plugin->options['map_settings']['width'];
      }
      if (isset($this->view->style_plugin->options['map_settings']['height'])) {
        $map['height'] = $this->view->style_plugin->options['map_settings']['height'];
      }
      if (isset($this->view->style_plugin->options['map_settings']['clustering']) && $this->view->style_plugin->options['map_settings']['clustering'] == 'markerclusterer_packed') {
        //$map['clustermarker'] = $this->view->style_plugin->options['map_settings']['clustermarker'];
        $map['clustering'] = $this->view->style_plugin->options['map_settings']['clustering'];
      }
      elseif (isset($this->view->style_plugin->options['map_settings']['clustering']) && $this->view->style_plugin->options['map_settings']['clustering'] == 'clusterer2') {
        //$map['clusterer'] = $this->view->style_plugin->options['map_settings']['clusterer'];
        $map['clustering'] = $this->view->style_plugin->options['map_settings']['clustering'];
        $map['maxVisibleMarkers'] = $this->view->style_plugin->options['map_settings']['max_visible_markers'];
        $map['minMarkersPerCluster'] = $this->view->style_plugin->options['map_settings']['min_markers_per_cluster'];
        $map['maxLinesPerBox'] = $this->view->style_plugin->options['map_settings']['max_lines_per_box'];
        
      }      
      if (isset($this->view->style_plugin->options['map_settings']['initial_state']['zoom'])) {
        $map['initial_state']['zoom'] = $this->view->style_plugin->options['map_settings']['initial_state']['zoom'];
      }
      if (isset($this->view->style_plugin->options['map_settings']['initial_state']['center'])) {
        $map['initial_state']['center'] = $this->view->style_plugin->options['map_settings']['initial_state']['center'];
      }
      if (isset($this->view->style_plugin->options['map_settings']['layers']['layer'])) {
        $map['layers']['layer'] = $this->view->style_plugin->options['map_settings']['layers']['layer'];
      }   
      if (isset($this->view->style_plugin->options['map_settings']['layers']['overlay'])) {
        $map['layers']['overlay'] = $this->view->style_plugin->options['map_settings']['layers']['overlay'];
      }
      if (isset($this->view->style_plugin->options['map_settings']['layers']['defaultlayer'])) {
        $map['layers']['defaultlayer'] = $this->view->style_plugin->options['map_settings']['layers']['defaultlayer'];        
      }            
      $map['view_link'] = 'admin/build/views/edit/' . $this->view->name . '?#views-tab-' . $this->view->current_display;
      $map['tagmap_title'] = $this->view->style_plugin->options['title'];
      $map['storage'] = '';
      $map['vidi_link'] = '';
      $output .= theme($this->theme_functions(), $this->view, $this->options, $map, $title);
    }
    else {
      drupal_set_message(t('The tagmap has no markers to display. Choose some content to see the tagmap.'), 'status');
    }
    return $output;
  }
  

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    module_load_include('inc', 'tagmap', 'tagmap_color_presets');
    $tagmap_color_presets = _tagmap_color_presets();
    $preset_start = array('' => 'From CSS file');
    $defaults = array_merge(tagmap_defaults(), variable_get('tagmap_settings', array()));
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Name your tagmap'),
      '#reqiured' => TRUE,
      '#default_value' => isset($this->options['title'])?$this->options['title']:'',
      '#maxlength' => 60,
      '#size' => 60,
    );
      
    $form['map_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Map settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['map_settings']['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#description' => t('Map width, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>.'),
        '#default_value' => (isset($this->options['map_settings']['width']) ? $this->options['map_settings']['width'] : $defaults['width']),
        '#maxlength' => 10,
        '#size' => 10,
        '#required' => TRUE,
    );

    $form['map_settings']['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#description' => t('Map height, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>.'),
        '#default_value' => (isset($this->options['map_settings']['height']) ? $this->options['map_settings']['height'] : $defaults['height']),
        '#maxlength' => 10,
        '#size' => 10,
        '#required' => TRUE,
    );

    $form['map_settings']['initial_state'] = array(
        '#type' => 'fieldset',
        '#title' => t('Set the initial state of the map'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#description' => t('These settings are optional. If you do not enter the values, they will be calculated on the basis of the markers position on the map.'),
    );

    $form['map_settings']['initial_state']['zoom'] = array(
        '#type' => 'select',
        '#title' => t('Default zoom'),
        '#default_value' => (isset($this->options['map_settings']['initial_state']['zoom']) ? $this->options['map_settings']['initial_state']['zoom'] : $defaults['initial_state']['zoom']),
        '#options' => array(-1 => t('None')) + drupal_map_assoc(range(0, 17)),
        '#description' => t('The default zoom level of the map.'),
    );

    $form['map_settings']['initial_state']['center'] = array(
        '#type' => 'textfield',
        '#title' => t('Default center'),
        '#default_value' => (isset($this->options['map_settings']['initial_state']['center']) ? $this->options['map_settings']['initial_state']['center'] : $defaults['initial_state']['center']),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('The default center coordinates of the map, expressed as a decimal latitude and longitude, separated by a comma.'),
        '#element_validate' => array('tagmap_center_validate'),
    );
    
    $form['map_settings']['layers'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add additional layers and overlays to the map'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Here you can add additional layers and/or overlays to the map. You have to define them ahead of time on the !administration page',
                           array('!administration' => user_access('administer site configuration') ? l('administration', 'admin/settings/tagmap/layers/addlayer', array('attributes' => array('target' => '_blank'))) : 'administration')),    
    );
    $results = db_query("SELECT * FROM {tagmap_layers}");
    $layers = array(); $overlays = array('' => 'None');
    $default_layers = array();
    while ($result = db_fetch_object($results)) {
      if ($result->baselayer) {
        $layers[$result->lid] = $result->title;
        $default_layers[$result->name] = $result->title;
      }
      else
        $overlays[$result->lid] = $result->title;
    }
    $form['map_settings']['layers']['layer'] = array(
      '#type' => 'select',
      '#title' => t('Select layers to add'),
      '#multiple' => TRUE,
      '#options' => $layers,
      '#default_value' => isset($this->options['map_settings']['layers']['layer']) ? $this->options['map_settings']['layers']['layer'] : '' 
    );
    $form['map_settings']['layers']['overlay'] = array(
      '#type' => 'select',
      '#title' => t('Select an overlay to add'),
      '#options' => $overlays,
      '#default_value' => isset($this->options['map_settings']['layers']['overlay']) ? $this->options['map_settings']['layers']['overlay'] : '' 
    );
    foreach ($defaults['baselayers'] as $key => $value) {
      if ($value)
        $default_layers[$key] = $key;
    }
    
    $form['map_settings']['layers']['defaultlayer'] = array(
      '#type' => 'select',
      '#title' => t('Select the default map layer'),
      '#options' => $default_layers,
      '#default_value' => isset($this->options['map_settings']['layers']['defaultlayer']) ? $this->options['map_settings']['layers']['defaultlayer'] : $defaults['maptype']
    );          

    if (file_exists(drupal_get_path('module', 'tagmap') .'/js/markerclusterer_packed.js') || file_exists(drupal_get_path('module', 'tagmap') .'/js/Clusterer2.js')) {
      $opts = array('' => 'None');
      if (file_exists(drupal_get_path('module', 'tagmap') .'/js/markerclusterer_packed.js'))
        $opts['markerclusterer_packed'] = 'markerclusterer_packed.js';
      if (file_exists(drupal_get_path('module', 'tagmap') .'/js/Clusterer2.js'))
        $opts['clusterer2'] = 'Clusterer2.js';      
      $form['map_settings']['clustering'] = array(
          '#type' => 'select',
          '#title' => t('Use clusters for marker management'),
          '#options' => $opts,
          '#default_value' => (isset($this->options['map_settings']['clustering']) ? $this->options['map_settings']['clustering'] : $defaults['clustering']),
      );
      
      $form['map_settings']['max_visible_markers'] = array(
          '#type' => 'textfield',
          '#title' => t('Maximum number of visible markers'),
          '#default_value' => isset($this->options['map_settings']['max_visible_markers'])?$this->options['map_settings']['max_visible_markers']:'150',
          '#size' => 10,
          '#maxlength' => 10,
          '#process' => array('views_process_dependency'),
          '#description' => t('The maximum number of visible markers when using Clusterer2.js library.'),
          '#dependency' => array('edit-style-options-map-settings-clustering' => array('clusterer2')),
          '#element_validate' => array('tagmap_positive_integer'),
      ); 
      $form['map_settings']['min_markers_per_cluster'] = array(
          '#type' => 'textfield',
          '#title' => t('Minimum number of markers per cluster'),
          '#default_value' => isset($this->options['map_settings']['min_markers_per_cluster'])?$this->options['map_settings']['min_markers_per_cluster']:'5',
          '#size' => 10,
          '#maxlength' => 10,
          '#process' => array('views_process_dependency'),
          '#description' => t('The minimum number of markers per cluster when using Clusterer2.js library.'),
          '#dependency' => array('edit-style-options-map-settings-clustering' => array('clusterer2')),
          '#element_validate' => array('tagmap_positive_integer'),
      );      
      $form['map_settings']['max_lines_per_box'] = array(
          '#type' => 'textfield',
          '#title' => t('Maximum number of lines'),
          '#default_value' => isset($this->options['map_settings']['max_lines_per_box'])?$this->options['map_settings']['max_lines_per_box']:'10',
          '#size' => 10,
          '#maxlength' => 10,
          '#process' => array('views_process_dependency'),
          '#description' => t('The maximum number of lines in the info box when using Clusterer2.js library.'),
          '#dependency' => array('edit-style-options-map-settings-clustering' => array('clusterer2')),
          '#element_validate' => array('tagmap_positive_integer'),
      );                       
    }  
      
    $form['map_settings']['embed_link'] = array(
      '#type' => 'checkbox',
      '#weight' => 4,
      '#title' => t('Display embed link'),
      '#return_value' => 1,
      '#default_value' => isset($this->options['map_settings']['embed_link']) ? $this->options['map_settings']['embed_link'] : '0',
    );           
    
    $form['steps'] = array(
        '#type' => 'textfield',
        '#title' => t('Tag levels'),
        '#description' => t('Number of different tag levels that will be generated. Must be an integer value.'),
        '#default_value' => (isset($this->options['steps']) ? $this->options['steps'] : 6),
        '#maxlength' => 10,
        '#size' => 10,
        '#required' => TRUE,
    );

    $form['fontsize'] = array(
        '#type' => 'textfield',
        '#title' => t('Font size in pixels'),
        '#description' => t('Minimum font size for the first level terms.'),
        '#default_value' => (isset($this->options['fontsize']) ? $this->options['fontsize'] : 10),
        '#maxlength' => 10,
        '#size' => 10,
        '#required' => TRUE,
    );
    
    $font_style = array('normal' => 'Normal', 'italic' => 'Italic', 'oblique' => 'Oblique');
    
    $form['fontstyle'] = array(
        '#type' => 'select',
        '#title' => t('Font style'),
        '#default_value' => (isset($this->options['fontstyle']) ? $this->options['fontstyle'] : $font_style['normal']),
        '#options' => $font_style,
        '#required' => TRUE,
    ); 
    
    $form['color'] = array(
        '#type' => 'select',
        '#title' => t('Color preset'),
        '#default_value' => isset($this->options['color']) ? $this->options['color'] : $preset_start[''],
        '#options' => $preset_start + $tagmap_color_presets,
    );       

    $supported_string_handlers = array('views_handler_field' , 'views_handler_field_taxonomy', 'views_handler_field_markup');
    $supported_numeric_handlers = array('views_handler_field_numeric', 'geotaxonomy_handler_field_term_node_count', 'location_views_handler_field_latitude', 'location_views_handler_field_longitude');
    $options = array('numerical' => array(), 'string' => array(), 'unsorted' => array());
    $handlers = $this->display->handler->get_handlers('field');
    foreach ($handlers as $field => $handler) {
      $handler_class = get_class($handler);
      if (in_array($handler_class, $supported_numeric_handlers)) {
        $options['numerical'][$field] = $handler->ui_name();
      }
      elseif (in_array($handler_class, $supported_string_handlers)) {
        $options['string'][$field] = $handler->ui_name();
      }
      else
        $options['unsorted'][$field] = $handler->ui_name();
    }

    $form['latfield'] = array(
        '#title' => t('Latitude field'),
        '#description' => t('Format must be degrees decimal.'),
        '#type' => 'select',
        '#options' => array_merge($options['numerical'], $options['unsorted']),
        '#default_value' => $this->options['latfield'],
        '#required' => TRUE,
    );

    $form['lonfield'] = array(
        '#title' => t('Longitude field'),
        '#description' => t('Format must be degrees decimal.'),
        '#type' => 'select',
        '#options' => array_merge($options['numerical'], $options['unsorted']),
        '#default_value' => $this->options['lonfield'],
        '#required' => TRUE,
    );

    $form['weightfield'] = array(
        '#title' => t('Tag weight field'),
        '#description' => t('If you don\'t choose a field, term weight will be calculated counting number of nodes assigned with the term.'),
        '#type' => 'select',
        '#options' => ($this->view->base_table != 'term_data' ? array_merge($options['numerical'], $options['unsorted']) : array_merge(array('' => 'None'), $options['numerical'], $options['unsorted'])),
        '#default_value' => $this->options['weightfield'],
    );
    
    $form['description_title'] = array(
        '#title' => t('Title'),
        '#description' => t('The title of the description cloud.'),
        '#type' => 'select',
        '#options' => array_merge(array('' => 'None'), $options['string'], $options['unsorted']),
        '#default_value' => $this->options['description_title'],
    );    
    
    $form['table'] = array(
        '#type' => 'fieldset',
        '#title' => t('Fields to be displayed in the info cloud and their order'),
        '#theme' => 'tagmap_rearrange_form',
        '#tree' => TRUE
    );

    $count = 0;
    $sort_array = array_merge($options['string'], $options['numerical'], $options['unsorted']);
    if (isset($this->options['table']) && count($this->options['table'] == count($sort_array))) {
      $sort_array = $this->options['table'];
      uasort($sort_array, '_tagmap_element_sort');
      $diff = array_diff_key($sort_array, array_merge($options['string'], $options['numerical'], $options['unsorted']));
      foreach ($diff as $key => $value)
        unset($sort_array[$key]);       
      
      $sort_array = array_keys($sort_array);
      $tmp = array();
      foreach ($sort_array as $value)
        $tmp[$value] = $value;
      $sort_array = $tmp; unset($tmp);
      foreach ($sort_array as $key => $value) {
        $handler = $this->display->handler->get_handler('field', $key);
        $sort_array[$key] = $handler->ui_name();
      }
      $diff = array_diff_assoc(array_merge($options['string'], $options['numerical'], $options['unsorted']), $sort_array);
      foreach ($diff as $key => $value)
        $sort_array[$key] = $value;
    }

    foreach ($sort_array as $id => $field) {
      $form['table'][$id] = array('#tree' => TRUE);
      $form['table'][$id]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 200,
        '#default_value' => ++$count,
      );


        $form['table'][$id]['name'] = array(
          '#value' => $field,
        );

      $form['table'][$id]['add'] = array(
        '#type' => 'checkbox',
        '#id' => 'views-removed-' . $id,
        '#attributes' => array('class' => 'tagmap-remove-checkbox'),
        '#default_value' => isset($this->options['table'][$id]['add'])?$this->options['table'][$id]['add']:0,
      );
    }     
    // Add javascript settings that will be added via $.extend for tabledragging
    $form['#js']['tableDrag']['arrange']['weight'][0] = array(
      'target' => 'weight',
      'source' => NULL,
      'relationship' => 'sibling',
      'action' => 'order',
      'hidden' => TRUE,
      'limit' => 0,
    );       

    if ($this->view->base_table != 'term_data') {
      $form['tagfield'] = array(
          '#title' => t('Tag field'),
          '#description' => t('String that will be displayed over map.'),
          '#type' => 'select',
          '#options' => array_merge(array('' => ''), $options['string'], $options['unsorted']),
          '#default_value' => $this->options['tagfield'],
          '#required' => TRUE,
      );

      if (module_exists('imagecache')) {
        $imagecache_presets = array('--' => t('--'));

        foreach (imagecache_presets() as $preset) {
          $imagecache_presets[$preset['presetname']] = $preset['presetname'];
        }

        $form['imagecache_icon'] = array(
            '#type' => 'select',
            '#title' => t('Imagecache preset for Image'),
            '#options' => $imagecache_presets,
            '#description' => t('Imagecache preeset. $Image dimensions should be in px. Not necessary, but recomended.'),
            '#default_value' => $this->options['imagecache_icon'],
        );

        $form['imagefield'] = array(
            '#title' => t('Image field'),
            '#type' => 'select',
            '#options' => array_merge(array('--' => '--'), $options['string'], $options['unsorted']),
            '#default_value' => $this->options['imagefield'],
            '#description' => t('This field should be excluded from display so the path to the image doesn\'t render.'),
        );
      }
    }
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    $title = trim($form_state['values']['style_options']['title']);
    if (empty($title)) {
      form_set_error('style_options][title', t('Field @field must not be empty.', array('@field' => $form['title']['#title'])));
    }
  
    if (!is_numeric($form_state['values']['style_options']['steps'])) {
      form_set_error('style_options][steps', t('Field @field must be integer.', array('@field' => $form['steps']['#title'])));
    }
    elseif ($form_state['values']['style_options']['steps'] <= 0) {
      form_set_error('style_options][steps', t('Field @field must be positive value.', array('@field' => $form['steps']['#title'])));
    }

    if (!is_numeric($form_state['values']['style_options']['fontsize'])) {
      form_set_error('style_options][fontsize', t('Field @field must be numeric.', array('@field' => $form['fontsize']['#title'])));
    }
    elseif ($form_state['values']['style_options']['fontsize'] <= 0) {
      form_set_error('style_options][fontsize', t('Field @field must be positive value.', array('@field' => $form['fontsize']['#title'])));
    }

    if (!tagmap_css_dimensions_validate($form_state['values']['style_options']['map_settings']['width'])) {
      form_set_error('style_options][map_settings][width', t('The specified value is not a valid CSS dimension.'));
    }

    if (!tagmap_css_dimensions_validate($form_state['values']['style_options']['map_settings']['height'], TRUE)) {
      form_set_error('style_options][map_settings][height', t('The specified value is not a valid CSS dimension or is a percentage.'));
    }
  }
}