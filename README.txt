
DESCRIPTION
-------------------------------------
Tagmap provides a Views style plug-in for displaying geo-coded tag cloud
over Google map. There are two possibilities: to display taxonomy terms, or
to display previously imported external data. 

REQUIREMENTS
-------------------------------------
- Drupal 6.x
- Views

INSTALLATION AND CONFIGURATION
-------------------------------------
1) Tagmap module requires Views module.

2) Customize the tagmap settings in Administer >> Site configuration >> Tagmap.

3) You will need a Google Maps API key for your website. You can get one at: 
http://www.google.com/apis/maps/signup.html
If you already have GMap module enabled, Tagmap module will inherit GMap setting
for Google Maps API key. You can override it, if you like.

4) Some of the settings can be overridden with views style plug-in options.

5) Recommended modules for use with Tagmap:
   - Geo Taxonomy
   - Views Group By

CLUSTERMARKER
-------------------------------------
As additional possibility you can use ClusterMarker to refine display. To be
able to use this option you have to download file markerclusterer_packed.js from

http://gmaps-utility-library.googlecode.com/svn/trunk/markerclusterer/1.0/src/

and place it into modules/tagmap/js directory.

After that go to configuration page or view and check the option to use cluster marker.

CLUSTERER
-------------------------------------
You can also use the Clusterer to group several close markers together. This library has
a nice feature to list previously defined number of markers in every given cluster.

Just go to 

http://www.acme.com/javascript/Clusterer2.jsm (for packed version) or

http://www.acme.com/javascript/Clusterer2.js (for human-friendly version)

and save the Clusterer2.js file in the modules/tagmap/js directory.