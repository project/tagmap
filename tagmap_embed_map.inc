<?php

/**
 * @file
 *
 */
 
/**
 * Form for admministration of embeded files
 * and confirmation form
 * @param $form_state
 */
function tagmap_admin_embeds_controller($form_state) {
  $checked = FALSE;
  if (!empty($form_state['values']['visualizations'])) {
    foreach ($form_state['values']['visualizations'] as $value) {
      if ($value) {
        $checked = TRUE;
        break;
      }
    }
  }
  if ($form_state['values']['op'] == 'Delete selected' && $checked) 
    return _tagmap_admin_embeds_delete_confirm($form_state);
    
  return _tagmap_admin_embeds_form($form_state);    
}

/**
 * Confirmation form for deleting the embed file
 * and corresponding database records
 * @param $form_state
 */
function _tagmap_admin_embeds_delete_confirm($form_state) {
  $desc = 'This action CAN NOT be undone.';
  // Tell the submit handler to process the form
  $form['process'] = array('#type' => 'hidden', '#value' => 'true');
  // Make sure the form redirects in the end
  $form['destination'] = array('#type' => 'hidden', '#value' => 'admin/content/tagmap/embeds');
  $to_delete = array();
  foreach ($form_state['values']['visualizations'] as $key => $value) {
    if ($value) {
      $to_delete[]=$value;
    }
  }
  $form['embeds_to_delete'] = array('#type' => 'hidden', '#value' => implode(',', $to_delete));
  return confirm_form($form,
                      t('Are you sure you want to delete the selected embed files?'),
                      'admin/content/tagmap/embeds',
                      $desc,
                      'Delete',
                      'Return');   
}

/**
 * Theme function for the the embed files table
 * @param $form
 */
function theme_tagmap_get_my_visualizations($form) {
  $select_header = theme('table_select_header_cell');
  $header = array(
    $select_header,
    t('Title'),
    t('Embed code'),
    t('Date published'),
//    t('Operations'),
  );
  if (module_exists('vidi'))
    $header[] = t('Operations');
  
  $output = '';
  $output .= drupal_render($form['options']);
  $rows = array();
  $counter = 0;
  foreach (element_children($form['title']) as $key) {
    $row = array();
    $row[] = drupal_render($form['visualizations'][$key]);
    $row[] = drupal_render($form['title'][$key]);
    $row[] = drupal_render($form['code'][$key]);    
    $row[] = drupal_render($form['date'][$key]);
    if (module_exists('vidi'))
      $row[] = drupal_render($form['operations'][$key]);
    $rows[] = $row;
    $counter++;
  }
  if ($counter == 0) {
    if (module_exists('vidi'))
      $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '5'));
    else
      $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '4'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Form for the administration of user created embed files.
 * Everyone who can make an embed file can administer their files.
 * @param $form_state
 */
function _tagmap_admin_embeds_form($form_state) {
  $limit = 20; 
  //If the form fail validation, remember the #action attribute
  //so it doesn't redirect to the ahah callback
  if (!isset($form_state['storage']['old_action'])) {
    $form_state['storage']['old_action'] = isset($form['#action']) ? $form['#action'] : request_uri();
  }  

  $script = "function popup(mylink, windowname){if (! window.focus)return true; var href; if (typeof(mylink) == 'string') href=mylink; else href=mylink.href; window.open(href, windowname, 'width=1000,height=800,scrollbars=yes');return false;}";
  drupal_add_js($script, 'inline');
  if (user_access('administer embeds')) {
    $form['filter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fiter embeds'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,      
    );
    $results = db_query('SELECT uid, name FROM {users}');
    $users = array();
    while ($result = db_fetch_object($results)) {
      $users[$result->uid] = $result->uid != 0 ? $result->name : "anonymous";
    }
    $form['filter']['per_user'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#default_value' => $form_state['values']['per_user'],
      '#title' => t('Fiter embeds'),
      '#options' => $users  
    );
    
    $form['filter_button'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
      '#ahah' => array(
        'path'    => 'tagmap/embed_ahah',
        'wrapper' => 'tagmap-embed-table-ahah',
        'method' => 'replace',
        'effect' => 'fade'
      ),           
    );
    if (isset($_GET['per_user']))
      $form_state['values']['per_user'] = $_GET['per_user'];
    $sql = "SELECT te.fid, te.title, te.link_vidi, te.visualisation_code, f.filename, f.filepath, f.timestamp FROM {tagmap_embeds} te LEFT JOIN {files} f ON te.fid = f.fid";
    if (!empty($form_state['values']['per_user'])) {
      $sql .= " WHERE";
      
      foreach ($form_state['values']['per_user'] as $value)
        $sql .= " f.uid = %d OR";
        
      $sql = substr($sql, 0, strlen($sql) - 2);
      $results = pager_query($sql, $limit, 0, NULL, $form_state['values']['per_user']);
    }
    else
      $results = pager_query($sql, $limit, 0, NULL);
  }
  else {  
    global $user;
    $sql = "SELECT te.fid, te.title, te.link_vidi, te.visualisation_code, f.filename, f.filepath, f.timestamp FROM {tagmap_embeds} te LEFT JOIN {files} f ON te.fid = f.fid WHERE f.uid = %d";
    $results = pager_query($sql, $limit, 0, NULL, $user->uid);  
  }
  
  $destination = drupal_get_destination();
  $visualizations = array();
  while ($result = db_fetch_object($results)) {
    $visualizations[$result->fid] = '';
    $form['data']['title'][$result->fid] = array('#value' => l($result->title, $result->filepath, array('attributes' => array('onClick' => 'return popup(this, \'My visualization\')'))));
    $form['data']['date'][$result->fid] =  array('#value' => format_date($result->timestamp));
    $form['data']['code'][$result->fid] =  array('#value' => $result->visualisation_code, '#prefix' => '<div style="font-size:9px;">', '#suffix' => '</div>');
    if (module_exists('vidi'))
      $form['data']['operations'][$result->fid] = array('#value' => l('edit', $result->link_vidi, array('query' => array('fid' => $result->fid))));
  }
  $form['data']['visualizations'] = array('#type' => 'checkboxes', '#options' => $visualizations);
  $form['data']['pager'] = array('#value' => theme('pager', NULL, $limit, 0));
  $form['data']['#theme'] = 'tagmap_get_my_visualizations';
  $form['data']['#prefix'] = '<div id="tagmap-embed-table-ahah">';
  $form['data']['#suffix'] = '</div>';
  $form['tagmap_delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected'),    
  );
  return $form;  
}

/**
 * Ahah callback for list of embeded files 
 * (filters the embeds by user)
 */
function tagmap_embed_ahah() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id); 

  $data = $form['data'];
  $_GET['q'] = 'admin/content/tagmap/embeds';
  $data['pager'] = array(
    '#value' => theme('pager', NULL, 3)
  );
  unset($data['#prefix'], $data['#suffix']);
  $output = theme('status_messages') . drupal_render($data);

  drupal_json(array('status' => TRUE, 'data' => $output)); 
}

/**
 * Submit handler for the embed administration form.
 * @param $form
 * @param $form_state
 */
function tagmap_admin_embeds_controller_submit($form, &$form_state) {
  if (!$form_state['clicked_button']['#id'] != 'edit-filter-button') {
    if (isset($form_state['values']['embeds_to_delete'])) {
      $for_deleting = explode(',', $form_state['values']['embeds_to_delete']);
      foreach ($for_deleting as $value) {
        $filepath = db_result(db_query("SELECT filepath FROM {files} WHERE fid=%d", $value));
        $sql = "DELETE FROM {tagmap_embeds} WHERE fid = %d";
        if (!db_query($sql, $value)) {
          drupal_set_message(t('An error occured while deleting the record with fid @id from the tagmap_embeds table!', array('@id' => $value)), 'error');
          return TRUE;
        }
        $sql = "DELETE FROM {files} WHERE fid = %d";
        if (!db_query($sql, $value)) {
          drupal_set_message(t('An error occured while deleting the record with fid @id  from the files table!', array('@id' => $value)), 'error');
          return TRUE;
        } 
        if (!file_delete($filepath)) {
          drupal_set_message(t('The file @path was NOT deleted!', array('@path' => $filepath)), 'error');
          return TRUE;
        }        
      }
      drupal_set_message(t('The file @path was deleted successfully!', array('@path' => $filepath)), 'status');
      return TRUE;
    }
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Adds the google map js in the embed file.
 */
function _tagmap_add_google_map_embed() {
  global $language;
  $query = array(
    'file' => 'api',
    'v' => variable_get('gmap_api_version', TAGMAP_GOOGLE_API_VERSION),
    'key' => variable_get('tagmap_api_key', ''),
    'hl' => $language->language,
  );  
  
  return '<script src="' . check_url(url('http://maps.google.com/maps', array('query' => $query))) . '" type="text/javascript"></script>';  
}

/**
 * @param $uid
 * @param $filename. The name of the embed file.
 * @param $filepath. Relative path of the embed file.
 * @param $storage. The state of the form from the VIDI module. Used to populate the form with correct data.
 * @param $vidi_link. link to the VIDI module form.
 * @param $view_link. Link to the corresponding view and display.
 * @param $result. Embed code.
 * @param $update. Is it an update or a creation of a new embed file.
 * @param $title. The title of the visualization. Displayed on the embed administration form.
 */
function _tagmap_save_file_data($uid, $filename, $filepath, $storage, $vidi_link, $view_link, $result, $update, $title) {
  $file = new stdClass();
  $file->uid = $uid;
  $file->filesize = filesize($filepath);
  $file->timestamp = time();
  
  if (!$update) {
    $file->filepath = $filepath;
    $file->filename = $filename;
    $file->filemime = file_get_mimetype($filepath);
    $file->status = 1;
    drupal_write_record('files', $file);
    $tagmap_embed_data = new stdClass();
    $tagmap_embed_data->fid = $file->fid;
    $tagmap_embed_data->title = $title;
    
    if (!empty($storage))
      $tagmap_embed_data->form_state_storage = $storage;
    if (!empty($vidi_link))
      $tagmap_embed_data->link_vidi = $vidi_link;
    if (!empty($view_link))
      $tagmap_embed_data->link_view = $view_link;
      
    $tagmap_embed_data->visualisation_code = $result;
    drupal_write_record('tagmap_embeds', $tagmap_embed_data);    
  }
  else {
    $sql = "SELECT fid FROM {files} WHERE filepath='%s'";
    $file->fid = db_result(db_query($sql, $filepath));
    drupal_write_record('files', $file, 'fid'); 
    $tagmap_embed_data = new stdClass();
    if (!empty($storage))
      $tagmap_embed_data->form_state_storage = $storage;       
    $tagmap_embed_data->fid = $file->fid;
    $tagmap_embed_data->visualisation_code = $result;
    $tagmap_embed_data->title = $title;
    drupal_write_record('tagmap_embeds', $tagmap_embed_data, 'fid');
  }
}

/**
 * Ajax calback that makes the embed file,
 * inserts corresponding data in the database
 * and returns the embed code to the user.
 */
function tagmap_embed() {
  global $user; 
  global $base_url;
  $id = $_POST['id']; 
  $rel = $_POST['rel']; 
  $title = $_POST['title']; 
  $clustering = $_POST['clustering']; 
  $settings = $_POST['settings'];
  if (empty($rel))
    return;
  $css = '<link type="text/css" rel="stylesheet" media="all" href="' . $base_url . '/' . drupal_get_path('module', 'tagmap') . '/tagmap.css"/>';
  //$js = drupal_get_js();
  $js = '<script type="text/javascript" src="' . $base_url . '/misc/jquery.js"></script>';
  $js .= '<script type="text/javascript" src="' . $base_url . '/misc/drupal.js"></script>';
  $js .= _tagmap_add_google_map_embed();
  if (module_exists('lightbox2')) {
    $js .= '<script type="text/javascript" src="' . $base_url . '/' . drupal_get_path('module', 'lightbox2') . '/js/lightbox.js"></script>';
    if (module_exists('flvmediaplayer') && module_exists('lightbox2') && variable_get('lightbox2_enable_video', 0))
      $js .= '<script src="' . $base_url . '/' . drupal_get_path('module', 'lightbox2') . '/js/lightbox_video.js?2" type="text/javascript"></script>';
    $css .= '<link type="text/css" rel="stylesheet" media="all" href="' . $base_url . '/' . drupal_get_path('module', 'lightbox2') . '/css/lightbox.css"/>';
  }
  $js .= '<script src="'. check_url(url('http://js.mapbox.com/g/2/mapbox.js')) .'" type="text/javascript"></script>';
  $js .= '<script type="text/javascript" src="' . $base_url . '/' . drupal_get_path('module', 'tagmap') . '/js/textmarker_marker.js"></script>';
  $js .= '<script type="text/javascript" src="' . $base_url . '/' . drupal_get_path('module', 'tagmap') . '/js/tagmap.js"></script>';

  if ($clustering == 'clusterer2')
    $js .= '<script type="text/javascript" src="' . $base_url . '/' . drupal_get_path('module', 'tagmap') . '/js/Clusterer2.js"></script>';
  if ($clustering == 'markerclusterer_packed')
    $js .= '<script type="text/javascript" src="' . $base_url . '/' . drupal_get_path('module', 'tagmap') . '/js/markerclusterer_packed.js"></script>';
  $js .= '<script type="text/javascript">' . $settings . '</script>';
  $output = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $css . $js . '<style>img, a img {border:none}</style></head>';
  $output .= '<body><div class="dropdown-processed"><div id="' . $rel . '" class="tagmap-embed tagmap-control" style="width:' . $_POST['width'] . '; height:' . $_POST['height'] . '">' . t('Javascript is required to view this map.') . '</div></div></body>';
  $output .= '</html>';
  $directory = file_directory_path() . '/tagmap_embed';
  file_check_directory($directory, TRUE);
  $directory = file_directory_path() . '/tagmap_embed/' . $user->uid;
  file_check_directory($directory, TRUE);
  if (empty($_POST['overwrite'])) {
    $filename = 'tagmap_embed' . '.html';
    $filepath = file_save_data($output, $directory . '/' . $filename, FILE_EXISTS_RENAME);
    $update = FALSE;
  }
  else {
    $filename = $_POST['overwrite'] . '.html';
    $filepath = file_save_data($output, $directory . '/' . $filename, FILE_EXISTS_REPLACE);
    $update = TRUE;
  }
  $result = "<div class=\"embed-instructions\"><strong>Embed link:</strong><br/>";
  $result .= "<div class=\"embed-code\" style=\" color: #666;\">";
  $code = "&lt;iframe scrolling = \"no\" width=\"" . $_POST['width'] . "\" height=\"" . $_POST['height'] . "\" src=\"";
  $code .= $base_url . "/";
  $code .= $filepath;
  $code .= "\"&gt;&lt;/iframe&gt";
  $result .= $code . "</div></div>";
  _tagmap_save_file_data($user->uid, $filename, $filepath, $_POST['storage'], $_POST['vidiLink'], $_POST['viewLink'], $code, $update, $title);
  echo $result;
}
