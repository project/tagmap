
/**
 * @file Tagmap integration in Drupal.
 */

/*
 * global $, Drupal, G_NORMAL_MAP, G_SATELLITE_MAP, G_HYBRID_MAP,
 * G_PHYSICAL_MAP,
 */
/*
 * global GMap2, GLatLng, GSmallZoomControl, GSmallMapControl, GLargeMapControl,
 * GMapTypeControl,
 */
/*
 * global GHierarchicalMapTypeControl, GMenuMapTypeControl, GLatLngBounds,
 * GIcon,
 */

Drupal.tagmap = new function() {
	var gmarkers = new Array();

	this.getMarker = function(mapnum_id, id) {
		return gmarkers[mapnum_id][id];
	};

	this.numberOfMarkers = function(mapnum_id) {
		return gmarkers[mapnum_id].length;
	};

	this.handleSelected = function(mapnum_id, opt) {
		var indexNo = opt[opt.selectedIndex].index;
		GEvent.trigger(gmarkers[mapnum_id][indexNo], "click");

	};
	
	this.addBonusLayers = function(mapid, map, mapTypes) {
		var default_set = false;
	  	var layers = Drupal.settings.tagmap[mapid].layer;
	  	var defaultlayer = Drupal.settings.tagmap[mapid].defaultlayer;
	  	if(layers != undefined && layers != null){
		  	for ( var i = 0; i <layers.length; i++) {
		  	    var custommap = GMapBox(layers[i].name, layers[i].title, layers[i].options);
		  	    map.addMapType(custommap);
		  	    if(defaultlayer == layers[i].name && !default_set){
		  	      map.setMapType(GMapBox(layers[i].name, layers[i].title, layers[i].options));
		  	      default_set = true;
		  	    }
		  	    
		  	}
	  	}
	  	var overlays = Drupal.settings.tagmap[mapid].overlay;
	  	if(overlays != undefined && overlays != null){
		  	for ( var i = 0; i <overlays.length; i++) {
		  	    var customoverlay  = GMapBox(overlays[i].name, overlays[i].title, overlays[i].options);
		  	    map.addOverlay(customoverlay);
		  	}
	  	}
	  	if (!default_set)
	  		map.setMapType(mapTypes[defaultlayer]);
	};

	/**
	 * Dropdown list with the tags as an option. On click, go to the tag on the
	 * map and open the info
	 */
	this.renderOptions = function(mapid, mapnum_id, id) {
		if (Drupal.settings.tagmap[mapid].clustering != 'clusterer2'
				&& Drupal.settings.tagmap[mapid].clustering != 'markerclusterer_packed'
				&& !$('#' + id).parent().hasClass('dropdown-processed')) {
			$('#' + id).parent().append(
					"<div id=\"tagmap-select-div-" + mapid
							+ "\" class=\"tagmap-select-div\"></div>")
			$("#tagmap-select-div-" + mapid)
					.append(
							"<select onchange=\"Drupal.tagmap.handleSelected("
									+ mapnum_id
									+ ", this);\" id=\"tagmap-select-select-options-"
									+ mapid
									+ "\" class=\"tagmap-select-select-options\"></select>")
			var len = Drupal.tagmap.numberOfMarkers(mapnum_id);
			for ( var i = 0; i < len; i++) {
				$("#tagmap-select-select-options-" + mapid).append(
						"<option  value=\"" + i + "\">"
								+ Drupal.tagmap.getMarker(mapnum_id, i).text_
								+ "</option>")
			}
			$('#' + id).parent().addClass('dropdown-processed');
		}
	};

	/**
	 * Encapsulation of marker creation. Needed so that events trigger correctly
	 */
	this.createMarker = function(lat, lon, opts, text) {
		var marker = new TextMarker(new GLatLng(lat, lon), opts);
		GEvent.addListener(marker, "click", function() {
			marker.openInfoWindowHtml(text);
		});
		return marker;
	};

	this.init = function() {
		var obj = this;
		if (GBrowserIsCompatible()) {
			var mapid = obj.id.split('-');
			mapid.pop();
			mapid.shift();
			var tmp = new String(mapid);
			var ind = tmp.indexOf("map");
			var mapnum_id = tmp.substr(4, ind - 4);
			mapid = mapid.join('-');
			obj.vars = Drupal.settings.tagmap[mapid];
			obj.vars.initial_state.zoom = parseInt(obj.vars.initial_state.zoom);
			var mapTypes = {
				'map' : G_NORMAL_MAP,
				'satellite' : G_SATELLITE_MAP,
				'hybrid' : G_HYBRID_MAP,
				'physical' : G_PHYSICAL_MAP
			};
			obj.vars.mapTypes = [];
			for (layer in obj.vars.baselayers) {
				obj.vars.mapTypes.push(mapTypes[layer]);
			}	
			var map = new GMap2(document.getElementById(obj.id), obj.vars);
			// Setting the map center. Check if user provided the default center
			// coordinates,
			// otherwise map center will be calculated on the basis of the
			// markers position on the map.
			var center = new GLatLng(0, 0);
			if (obj.vars.initial_state.center) {
				center = obj.vars.initial_state.center.split(',');
				center = new GLatLng(center[0], center[1]);
			}
			map.setCenter(center);

			
			
			// Setting map control type
			if (obj.vars.controltype === 'Micro') {
				map.addControl(new GSmallZoomControl());
			} else if (obj.vars.controltype === 'Small') {
				map.addControl(new GSmallMapControl());
			} else if (obj.vars.controltype === 'Large') {
				map.addControl(new GLargeMapControl());
			}
			// Setting map menu type control
			if (obj.vars.mtc === 'standard') {
				map.addControl(new GMapTypeControl());
			} else if (obj.vars.mtc === 'hier') {
				map.addControl(new GHierarchicalMapTypeControl());
			} else if (obj.vars.mtc === 'menu') {
				map.addControl(new GMenuMapTypeControl());
			}
			// Setting scrollwheel and continuous zoom
			if (!obj.vars.behavior.nomousezoom) {
				map.enableScrollWheelZoom();
				map.enableContinuousZoom();
			}

			//map.setMapType(mapTypes[obj.vars.maptype]);
			Drupal.tagmap.addBonusLayers(mapid, map, mapTypes);
			
			// Generation of Text markers
			var cluster = [];
			var bounds = new GLatLngBounds;
			gmarkers[mapnum_id] = new Array();
			if (obj.vars.clustering == 'clusterer2') {
				var clusterer = new Clusterer(map);
				clusterer.SetMaxVisibleMarkers(Drupal.settings.tagmap[mapid].maxVisibleMarkers);
				clusterer.SetMinMarkersPerCluster(Drupal.settings.tagmap[mapid].minMarkersPerCluster);
				clusterer.SetMaxLinesPerInfoBox(Drupal.settings.tagmap[mapid].maxLinesPerBox);
			}

			for ( var i = 0; i < obj.vars.markers.length; i++) {
				var opts = {};
				opts.text = obj.vars.markers[i].term;
				opts.weight = obj.vars.markers[i].weight;
				opts.icon = new GIcon(G_DEFAULT_ICON);
				opts.infoText = obj.vars.markers[i].infoText;
				opts.minFontSize = obj.vars.markers[i].minFontSize;
				opts.fontStyle = obj.vars.markers[i].fontStyle;
				opts.markerColor = obj.vars.markers[i].markerColor;
				var marker = Drupal.tagmap.createMarker(
						obj.vars.markers[i].latitude,
						obj.vars.markers[i].longitude, opts,
						obj.vars.markers[i].infoText);
				gmarkers[mapnum_id].push(marker);
				if (obj.vars.clustering == 'markerclusterer_packed') {
					cluster.push(marker);
				} else if (obj.vars.clustering == 'clusterer2') {
					clusterer.AddMarker(marker, obj.vars.markers[i].term);
				} else {
					map.addOverlay(marker);
				}

				bounds.extend(marker.getPoint());
			}
			if (obj.vars.clustering == 'markerclusterer_packed') {
				MarkerClusterer(map, cluster);
			}

			// If the default zoom level isn't provided, calculate the zoom
			// level based on
			// the markers position on the map, otherwise use the provided
			// value.
			if (obj.vars.initial_state.zoom == -1) {
				map.setZoom(map.getBoundsZoomLevel(bounds));
			} else {
				map.setZoom(obj.vars.initial_state.zoom);
			}

			// If the default map center coordinates aren't provided set
			// the map center based on the markers position on the map.
			if (!obj.vars.initial_state.center) {
				map.setCenter(bounds.getCenter());
			}
			Drupal.tagmap.renderOptions(mapid, mapnum_id, this.id);
		}
	};

	this.embed = function(id, rel, overwrite) {
		if (GBrowserIsCompatible()) {
			var mapid = rel.split('-');
			mapid.pop();
			mapid.shift();
			var width = Drupal.settings.tagmap[mapid].width;
			var height = Drupal.settings.tagmap[mapid].height;
			var scripts = document.getElementsByTagName('script');
			var scrString = "";
			for ( var i = 0; i < scripts.length; i++) {
				var tmp = scripts.item(i).text;
				if (tmp.search('tagmap') != -1 && tmp.search('infoText') != -1)
					scrString = tmp;

			}
			var path = Drupal.settings.basePath + 'tagmap/embed';
			$.ajax( {
				type : "POST",
				url : path,
				data : "id=" + id + "&rel=" + rel + "&clustering="
						+ Drupal.settings.tagmap[mapid].clustering
						+ "&settings=" + scrString+"&width="+width
						+"&height="+height+"&storage="+Drupal.settings.tagmap[mapid].storage+"&title="+Drupal.settings.tagmap[mapid].tagmap_title
						+"&vidiLink="+Drupal.settings.tagmap[mapid].vidi_link+"&viewLink="+Drupal.settings.tagmap[mapid].view_link+"&overwrite="+overwrite,
				success : function(msg) {
					var divid = $('#' + id).parent().attr('id');
					$('#' + divid).hide('fast');
					$('#' + divid).html(msg);
					$('#' + divid).fadeIn('slow');
				},
				error : function(msg) {
					alert('greska');
				}
			});
		}
	};
};

Drupal.behaviors.tagmap = function(context) {
	$('.tagmap-control:not(.tagmap-processed)', context).addClass(
			'tagmap-processed').each(Drupal.tagmap.init);
	$('.tagmap-embed-link', context).click(function() {
		Drupal.tagmap.embed($(this).attr('id'), $(this).attr('rel'), $(this).attr('overwrite'))
	});
}

//jQuery
//		.extend(
//				Drupal.settings,
//				{
//					"basePath" : "/acquia-drupal-1.2.24/",
//					"admin_menu" : {
//						"margin_top" : 1
//					},
//					"lightbox2" : {
//						"rtl" : 0,
//						"file_path" : "/acquia-drupal-1.2.24/(\\w\\w/)sites/default/files",
//						"default_image" : "/acquia-drupal-1.2.24/modules/acquia/lightbox2/images/brokenimage.jpg",
//						"border_size" : "10",
//						"font_color" : "000",
//						"box_color" : "fff",
//						"top_position" : "",
//						"overlay_opacity" : "0.8",
//						"overlay_color" : "000",
//						"disable_close_click" : 1,
//						"resize_sequence" : "0",
//						"resize_speed" : 400,
//						"fade_in_speed" : 400,
//						"slide_down_speed" : 600,
//						"use_alt_layout" : 0,
//						"disable_resize" : 0,
//						"disable_zoom" : 0,
//						"force_show_nav" : 0,
//						"loop_items" : 0,
//						"node_link_text" : "View Image Details",
//						"node_link_target" : 0,
//						"image_count" : "Image !current of !total",
//						"video_count" : "Video !current of !total",
//						"page_count" : "Page !current of !total",
//						"lite_press_x_close" : "press \x3ca href=\"#\" onclick=\"hideLightbox(); return FALSE;\"\x3e\x3ckbd\x3ex\x3c/kbd\x3e\x3c/a\x3e to close",
//						"download_link_text" : "Download Original",
//						"enable_login" : false,
//						"enable_contact" : false,
//						"keys_close" : "c x 27",
//						"keys_previous" : "p 37",
//						"keys_next" : "n 39",
//						"keys_zoom" : "z",
//						"keys_play_pause" : "32",
//						"display_image_size" : "",
//						"image_node_sizes" : "()",
//						"trigger_lightbox_classes" : "",
//						"trigger_lightbox_group_classes" : "",
//						"trigger_slideshow_classes" : "",
//						"trigger_lightframe_classes" : "",
//						"trigger_lightframe_group_classes" : "",
//						"custom_class_handler" : 0,
//						"custom_trigger_classes" : "",
//						"disable_for_gallery_lists" : true,
//						"disable_for_acidfree_gallery_lists" : true,
//						"enable_acidfree_videos" : true,
//						"slideshow_interval" : 5000,
//						"slideshow_automatic_start" : true,
//						"slideshow_automatic_exit" : true,
//						"show_play_pause" : true,
//						"pause_on_next_click" : false,
//						"pause_on_previous_click" : true,
//						"loop_slides" : false,
//						"iframe_width" : 600,
//						"iframe_height" : 400,
//						"iframe_border" : 1,
//						"enable_video" : 1,
//						"flvPlayer" : "sites/all/modules/flvmediaplayer/mediaplayer.swf",
//						"flvFlashvars" : ""
//					},
//					"CTools" : {
//						"pageId" : "page-763930c79120accbf3bf535c50414e39"
//					},
//					"tableDrag" : {
//						"arrange" : {
//							"weight" : [ {
//								"target" : "weight",
//								"source" : "weight",
//								"relationship" : "sibling",
//								"action" : "order",
//								"hidden" : true,
//								"limit" : 0
//							}, {
//								"target" : "weight",
//								"source" : "weight",
//								"relationship" : "sibling",
//								"action" : "order",
//								"hidden" : true,
//								"limit" : 0
//							} ]
//						}
//					},
//					"tagmap" : {
//						"auto0map" : {
//							"width" : "800px",
//							"height" : "600px",
//							"initial_state" : {
//								"zoom" : "-1",
//								"center" : ""
//							},
//							"controltype" : "Small",
//							"maptype" : "map",
//							"mtc" : "standard",
//							"baselayers" : {
//								"map" : 1,
//								"satellite" : 1,
//								"hybrid" : 1,
//								"physical" : 1
//							},
//							"behavior" : {
//								"nomousezoom" : 0
//							},
//							"clustering" : "",
//							"markers" : [
//									{
//										"latitude" : 3,
//										"longitude" : 3,
//										"term" : "Atlanta Hawks",
//										"weight" : 1,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/atlanta_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/atlanta_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 4,
//										"longitude" : 4,
//										"term" : "Boston Celtics",
//										"weight" : 2,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/boston_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/boston_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 7,
//										"longitude" : 7,
//										"term" : "Charlotte Bobcats",
//										"weight" : 3,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/charlotte_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/charlotte_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 8,
//										"longitude" : 8,
//										"term" : "Chicago Bulls",
//										"weight" : 3,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/chicago_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/chicago_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 1,
//										"longitude" : 1,
//										"term" : "Cleveland Cavaliers",
//										"weight" : 1,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"http://www.youtube.com/watch?v=j8vS7IrcNfo\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/http://www.youtube.com/watch?v=j8vS7IrcNfo\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 12,
//										"longitude" : 12,
//										"term" : "Detroit Pistons",
//										"weight" : 5,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/detroit_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/detroit_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 10,
//										"longitude" : 10,
//										"term" : "Indiana Pacers",
//										"weight" : 4,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/indiana_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/indiana_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 5,
//										"longitude" : 5,
//										"term" : "Miami Heat",
//										"weight" : 2,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/miami_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/miami_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 6,
//										"longitude" : 6,
//										"term" : "Milwaukee Bucks",
//										"weight" : 3,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/milwaukee_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/milwaukee_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 15,
//										"longitude" : 15,
//										"term" : "New Jersey Nets",
//										"weight" : 6,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/newjersey_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/newjersey_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 11,
//										"longitude" : 11,
//										"term" : "New York Knicks",
//										"weight" : 5,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/newyork_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/newyork_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 2,
//										"longitude" : 2,
//										"term" : "Orlando Magic",
//										"weight" : 1,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/orlando_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/orlando_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 13,
//										"longitude" : 13,
//										"term" : "Philadelphia 76ERS",
//										"weight" : 6,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/philadelphia_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/philadelphia_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 9,
//										"longitude" : 9,
//										"term" : "Toronto Raptors",
//										"weight" : 4,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/toronto_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/toronto_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									},
//									{
//										"latitude" : 14,
//										"longitude" : 14,
//										"term" : "Washington Wizards",
//										"weight" : 6,
//										"infoText" : "\x3cdiv style=\"width:60px; height:60px; \" class=\"tagmap-cloud-image\"\x3e\x3ca href=\"/acquia-drupal-1.2.24/sites/default/files/east_conference_new_importer_files/washington_0.jpeg\" onclick=\"Lightbox.start(this, false, false, false, false); return false;\" class=\"lightbox-processed\"\x3e\x3cimg src=\"/acquia-drupal-1.2.24/sites/default/files/imagecache/thumb_tagmap/east_conference_new_importer_files/washington_0.jpeg\"\x3e\x3c/img\x3e\x3c/a\x3e\x3c/div\x3e\x3cdiv class=\"tagmap-view-info-content\"\x3e\x3c/div\x3e\x3c/div\x3e",
//										"minFontSize" : "10",
//										"fontStyle" : "normal",
//										"markerColor" : "css"
//									} ],
//							"embed_link" : 1,
//							"view_link" : "admin/build/views/edit/vidi_import_east_conference?#views-tab-page_6",
//							"tagmap_title" : "eartaert",
//							"storage" : "",
//							"vidi_link" : "",
//							"id" : "auto0map",
//							"layer" : [ {
//								"name" : "world-print",
//								"title" : "World Print",
//								"options" : {
//									"minZoom" : "0",
//									"maxZoom" : "19"
//								}
//							}, {
//								"name" : "world-light",
//								"title" : "World Light",
//								"options" : {
//									"minZoom" : "0",
//									"maxZoom" : "11"
//								}
//							}, {
//								"name" : "world-dark",
//								"title" : "World Dark",
//								"options" : {
//									"minZoom" : "0",
//									"maxZoom" : "11"
//								}
//							} ],
//							"defaultlayer" : "world-light"
//						}
//					}
//				});