/**
 * @name TextMarker
 * @version 1.0
  * @fileoverview 
 * This library displays a text marker.
 */

 /**
 * @name TextMarkerOptions
 * @class This class represents optional arguments to {@link TextMarker} and 
 *   <code>GMarker</code>. 
 *   It has no constructor, but is instantiated as an object literal.
 * @property {String} [text = ""] Specifies the text shown on the map.
 * @property {String} [infoText = ""]  Specifies the text shown in the info window of the marker.
 * @property {String} [minFontSize = "12"]  Specifies the font size of the text on the lowest level.
 * @property {Float} [fontSizeRatio = 0.25]  Specifies the font size ratio.
 * @property {String} [containerID = "textmarker"]  Specifies the id of the container div.
 * @property {Integer} [weight = 1]  Specifies the weight of the marker.
 * @property {GIcon} Specifies the transparent marker icon. 
 */
 
/**
 * @desc Creates a marker with options specified in {@link TextMarkerOptions}
 *      (extension of <code>GMarkerOptions</code>). Creates a text marker and then
 *       calls the <code>GMarker</code> constructor.
 * @param {GLatLng} latlng Initial marker position
 * @param {TextMarkerOptions} [opts] Named optional arguments.
 * @constructor
 */
function TextMarker(latlng, opts) {
  this.latlng_ = latlng;
  opts = opts || {};
  this.text_ = opts.text || "";
  if (opts.text) {
    opts.text = undefined;
  }
  
  this.infoText = opts.infoText;
  if (opts.infoText) {
    opts.infoText = undefined;
  }
  
  this.opts_ = opts;
  
  this.minFontSize = opts.minFontSize || "12";
  this.fontSizeRatio = opts.fontSizeRatio || 0.25;
  
  this.containerID = opts.id || "textmarker";
  this.weight = opts.weight || 1;
  this.fontStyle = opts.fontStyle;
  this.markerColor = opts.markerColor;
  
  this.icon = opts.icon;
  this.icon.shadow = null;
  this.icon.iconSize  = new GSize(0,0);
  this.icon.shadowSize  = new GSize(0,0);
  this.icon.image = "";
  this.icon.iconAnchor = new GPoint(0,0);

  GMarker.apply(this, arguments);
}

/**
 * @private
 */
TextMarker.prototype = new GMarker(new GLatLng(0, 0));

/**
 * @desc Initialize the marker
 * @private
 */
TextMarker.prototype.initialize = function (map) {
  GMarker.prototype.initialize.apply(this, arguments);
  this.map_ = map;
  //  Make the container 
  this.container_ = document.createElement("div");
  if (this.isIE_() === true && this.getInternetExplorerVersion_() < 8) {
    this.container_.setAttribute('className', this.containerID + ' level' + this.weight); // ADD CLASSNAME BASED ON WEIGHT 
  }
  else {
    this.container_.setAttribute('class', this.containerID + ' level' + this.weight); // ADD CLASSNAME BASED ON WEIGHT 
  }
  map.getPane(G_MAP_MARKER_PANE).appendChild(this.container_);
  this.container_.style.zIndex = GOverlay.getZIndex(this.latlng_.lat());
  this.container_.style.position = "absolute";
  this.container_.style.visibility = "hidden";
  
    this.displayText_();
    
  // Events
  var this_ = this; 
  GEvent.bindDom(this.container_, "click", this, function () {    
	this.openInfoWindowHtml(this.infoText, { maxWidth: this.map_.getSize().width/2 });
  });
};

/**
 * @desc Create container with text 
 * @private
 */
TextMarker.prototype.displayText_ = function () {

  //Make text container
  this.bodyContainer_ = document.createElement("div");
  // IE 7 and less versions recognize className instead of class!
  if (this.isIE_() === true && this.getInternetExplorerVersion_() < 8) {
    this.bodyContainer_.setAttribute('className', this.containerID + '_body');
  }
  else {
    this.bodyContainer_.setAttribute('class', this.containerID + '_body');
  }
  this.bodyContainer_.style.position = "absolute";
  this.bodyContainer_.style.backgroundColor = "transparent";
  this.bodyContainer_.style.overflow = "hidden";
  this.container_.appendChild(this.bodyContainer_);
};

/**
 * @ignore
 */
TextMarker.prototype.redraw = function (force) {
  GMarker.prototype.redraw.apply(this, arguments);
  
  if (force) {
    this.showPopup();
    this.latlng_ = this.getLatLng();
    this.container_.style.zIndex = GOverlay.getZIndex(this.latlng_.lat());
  }
};

/**
 * @ignore
 */
TextMarker.prototype.copy = function () {
  this.opts_.text = this.text_;
  return new TextMarker(this.latlng_, this.opts_);
};

/**
 * @desc Hides the marker.
 */
TextMarker.prototype.hide = function () {
  GMarker.prototype.hide.apply(this, arguments);
  this.container_.style.visibility = "hidden";
};

/**
 * @desc Shows the marker.
 *    Note that this method shows only the marker.
 */
TextMarker.prototype.show = function () {
  GMarker.prototype.show.apply(this, arguments);
};

/**
 * @desc Shows the marker and the text.
 */
TextMarker.prototype.showPopup = function () {
  this.show();
  
  this.redrawText_(this.text_);
    
  var info = this.map_.getInfoWindow();
  if (!info.isHidden() || this.isNull(this.text_)) {
    return;
  }
  this.container_.style.visibility = "visible";
};

/**
 * @ignore
 */
TextMarker.prototype.remove = function () {
  GEvent.clearInstanceListeners(this.container_);
  while (this.container_.firstChild) {
    this.container_.removeChild(this.container_.firstChild);
  }
  this.container_.parentNode.removeChild(this.container_);
  GMarker.prototype.remove.apply(this, arguments);
  delete arguments.callee;
};

/**
 * @desc Redraws the text.
 * @private
 * @ignore
 */
TextMarker.prototype.redrawText_ = function (text) {  
  this.bodyContainer_.style.fontSize = (((this.weight-1) * this.fontSizeRatio + 1) * this.minFontSize) + "px";
  this.bodyContainer_.style.fontStyle = this.fontStyle;
  if (this.markerColor != 'css')
	this.bodyContainer_.style.color = this.markerColor;
  while (this.bodyContainer_.firstChild) {
    this.bodyContainer_.removeChild(this.bodyContainer_.firstChild);
  }
  this.bodyContainer_.innerHTML = text;
  if (this.isIE_() === false && this.bodyContainer_.hasChildNodes) {
    if (this.bodyContainer_.firstChild.nodeType === 1) {
      this.bodyContainer_.firstChild.style.margin = 0;
    }
  }	
  var offsetBorder = this.isIE_() ? 2 : 0;
  this.size_ = this.getHtmlSize_(this.bodyContainer_);
  this.container_.style.width = this.size_.width + "px";
  this.container_.style.height = this.size_.height + "px";
  var pxPos = this.map_.fromLatLngToDivPixel(this.latlng_);
   
  this.container_.style.left = (pxPos.x - (this.size_.width/2)) + "px";
  this.container_.style.top = (pxPos.y - this.size_.height/2) + "px";
};

/**
 * @private
 * @desc      detect null,null string and undefined
 * @param     value
 * @return    true  if value is nothing or false if value is not nothing
 */
TextMarker.prototype.isNull = function (value) {
  if (!value && value !== 0 ||
     value === undefined ||
     value === "" ||
     value === null ||
     typeof value === "undefined") {
    return true;
  }
  return false;
};

/**
 * @private
 * @desc return size of html elements
 * @param html : html elements
 * @return GSize
 */
TextMarker.prototype.getHtmlSize_ = function (el) {
  var mapContainer = this.map_.getContainer();
  var dummyTextNode = document.createElement("div");
  dummyTextNode.innerHTML = el.innerHTML;
  dummyTextNode.style.display = "inline";
  dummyTextNode.style.position = "absolute";
  dummyTextNode.style.fontSize = el.style.fontSize; 
  mapContainer.appendChild(dummyTextNode);
  
  var totalSize = new GSize(1, 1); // "1" is margin
  totalSize.width = dummyTextNode.offsetWidth;
  totalSize.height = dummyTextNode.offsetHeight;
  mapContainer.removeChild(dummyTextNode);
  
  return totalSize;
};

/**
 * @private
 * @desc      detect IE
 * @param     none
 * @return    true  if browser is Interner Explorer or false if browser is not Interner Explorer
 */
TextMarker.prototype.isIE_ = function () {
  return (navigator.userAgent.toLowerCase().indexOf('msie') !== -1) ? true : false;
};

/**
 * @private
 * @desc       Returns the version of Internet Explorer or a -1
 * @param     none
 * @return   version of Internet Explorer or a -1 (indicating the use of another browser).
 */
TextMarker.prototype.getInternetExplorerVersion_ = function() {
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer') {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}