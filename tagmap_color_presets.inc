<?php

/**
 * @file
 *
 */
 
/**
 * Returns a single preset or an array og presets
 * @param $prid
 */
function _tagmap_color_presets($prid = NULL) {
  $results = NULL;
  if (empty($prid))
    $results = db_query('SELECT * FROM {tagmap_preset}');
  else
    $results = db_query('SELECT * FROM {tagmap_preset} WHERE prid = %d ORDER BY preset_name', $prid);
  $presets=array();
  while ($result = db_fetch_object($results)) {
    $presets[$result->prid] = $result->preset_name;
  }
  return $presets;
}

/**
 * Returns all the colors that belong to the selected color preset
 * @param $prid
 */
function _tagmap_colors($prid) {
  $results = db_query('SELECT color_index, color_code FROM {tagmap_preset_color} WHERE prid = %d ORDER BY color_index', $prid);
  $colors = array();
  while ($result = db_fetch_object($results)) {
    $colors[$result->color_index] = $result->color_code;
  }
  return $colors;
}

/**
 * The controller form alternating between the preset list and the confirmation form
 * @param $form_state
 */
function tagmap_preset_list_controller($form_state) {
  $checked = FALSE;
  if (!empty($form_state['values']['presets'])) {
    foreach ($form_state['values']['presets'] as $value) {
      if ($value) {
        $checked = TRUE;
        break;
      }
    }
  }
  if ($form_state['values']['op'] == 'Delete selected' && $checked) 
    return _tagmap_preset_delete_confirm($form_state);
    
  return _tagmap_preset_list_form($form_state);      
}

/**
 * The confirmation form for preset removal
 * @param $form_state
 */
function _tagmap_preset_delete_confirm($form_state) {
  $desc = 'This action CAN NOT be undone.';
  $form['process'] = array('#type' => 'hidden', '#value' => 'true');
  $form['destination'] = array('#type' => 'hidden', '#value' => 'admin/settings/tagmap/presets/presetlist');
  $to_delete = array();
  foreach ($form_state['values']['presets'] as $key => $value) {
    if ($value) {
      $to_delete[]=$value;
    }
  }
  $form['presets_to_delete'] = array('#type' => 'hidden', '#value' => implode(',', $to_delete));
  return confirm_form($form,
                      t('Are you sure you want to delete the selected presets?'),
                      'admin/settings/tagmap/presets/presetlist',
                      $desc,
                      'Delete',
                      'Return');   
}

/**
 * Delete the preset
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_list_controller_submit($form, &$form_state) {
  if (isset($form_state['values']['presets_to_delete'])) {
    $for_deleting = explode(',', $form_state['values']['presets_to_delete']);
    foreach ($for_deleting as $value) {
      if (!db_query("DELETE FROM {tagmap_preset} WHERE prid = %d", $value)) {
        drupal_set_message(t('An error occured while deleting the one of the selected presets!'), 'error');
        return TRUE;
      }
      if (!db_query("DELETE FROM {tagmap_preset_color} WHERE prid = %d", $value)) {
        drupal_set_message(t('An error occured while deleting the colors of one of the selected presets!'), 'error');
        return TRUE;
      }             
    }
    drupal_set_message(t('The selected presets were deleted successfully!'), 'status');
    return TRUE;        
  }  
  $form_state['rebuild'] = TRUE;
}

/**
 * Theme for the table where the existing presets are listed
 * @param $form
 */
function theme_tagmap_list_presets($form) {
  $select_header = theme('table_select_header_cell');
  $header = array(
    $select_header,
    t('Preset name'),
    t('Colors'),
    t('Operations'),
  );
  
  $output = '';
  $output .= drupal_render($form['options']);
  $rows = array();
  $counter = 0;
  foreach (element_children($form['title']) as $key) {
    $row = array();
    $row[] = drupal_render($form['presets'][$key]);
    $row[] = drupal_render($form['title'][$key]);
    $row[] = drupal_render($form['colors'][$key]);    
    $row[] = drupal_render($form['operations'][$key]);
    $rows[] = $row;
    $counter++;
  }
  if ($counter == 0) {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '4'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;    
}

/**
 * Form where the existing presetsare listed
 * @param $form_state
 */
function _tagmap_preset_list_form($form_state) {
  $limit = 20;
  $sql = 'SELECT * FROM {tagmap_preset} ORDER BY preset_name';
  $results = pager_query($sql, $limit, 0);
  $presets = array();
  while ($result = db_fetch_object($results)) {
    $presets[$result->prid] = '';
    $form['data']['title'][$result->prid] = array('#value' => $result->preset_name);
    $colors = db_query('SELECT color_code, color_index FROM {tagmap_preset_color} WHERE prid=%d ORDER BY color_index', $result->prid);
    $counter = 0;
    $output = '';
    while ($color = db_fetch_object($colors)) {   
      $output .= '<div style="display:inline; padding-right:10px; margin-right:3px; width:10px; height:10px; border:1px solid; background:' . $color->color_code . '" title="' . $color->color_code . '"></div>';
      $counter++;    
      if (($counter%15) == 0) 
        $output .= '<br/>';
    }
    $form['data']['colors'][$result->prid] =  array('#value' => $output);
    $form['data']['operations'][$result->prid] = array('#value' => l('edit', 'admin/settings/tagmap/edit/preset/' . $result->prid) . ' | ' . l('export', 'admin/settings/tagmap/export/preset/' . $result->prid));
  }   
  $form['data']['presets'] = array('#type' => 'checkboxes', '#options' => $presets);
  $form['data']['pager'] = array('#value' => theme('pager', NULL, $limit, 0));
  $form['data']['#theme'] = 'tagmap_list_presets';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected'),    
  );  
  $form['new_preset'] = array(
    '#value' => l('Add new color preset', 'admin/settings/tagmap/presets/new_preset'),
  );
  return $form;
}

/**
 * Ahah callback function for adding or editing apreset 
 * (this one adds a new color field)
 */
function tagmap_presets_js() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id); 

  $preset_colors = $form['preset_colors']['colors'];
  unset($preset_colors['#prefix'], $preset_colors['#suffix']);
  $output = theme('status_messages') . drupal_render($preset_colors);

  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Create new preset form
 * @param $form_state
 */
function tagmap_preset_form($form_state) {
  if (!module_exists('colorpicker') && $_GET['q'] == 'admin/settings/tagmap/presets/new_preset')
    drupal_set_message(t('For additional functionality install Colorpicker module from !link', array('!link' => l('here', 'http://drupal.org/project/colorpicker', array('attributes' => array('target' => '_blank'))))), 'warning');
  
  //If the form fail validation, remember the #action attribute
  //so it doesn't redirect to the ahah callback
  if (!isset($form_state['storage']['old_action'])) {
    $form_state['storage']['old_action'] = isset($form['#action']) ? $form['#action'] : request_uri();
  }
  // If the form action change, restore the old one.
  if (isset($form_state['storage']['old_action']) && 
      ($form['#action']!=$form_state['storage']['old_action'])) {
    $form['#action'] = $form_state['storage']['old_action'];
  }   

  drupal_add_css(drupal_get_path('module', 'tagmap') . '/tagmap.css');
  
  $form['name_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preset name'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );  
  $form['name_fieldset']['name'] = array(
    '#type' => 'textfield',
//    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['name'])?$form_state['values']['name']:''
  );
  
  $form['preset_colors'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#attributes' => array('class' => 'ahah-wrapper'),
    '#title' => t('Color codes'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['preset_colors']['colors'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="tagmap-colors">',
    '#suffix' => '</div>',
  );
  
  $color_count = isset($form_state['count']) ? $form_state['count'] : 1;
  for ($delta = 0; $delta < $color_count; $delta++) {
    if (module_exists('colorpicker')) {
      $form['preset_colors']['colors'][$delta] = array(
        '#type' => 'colorpicker_textfield',
        '#field_prefix' => '#',
        '#title' => t('Color ' . ($delta+1)),
        '#default_value' => isset($form_state['values']['preset_colors']['colors'][$delta])?$form_state['values']['preset_colors']['colors'][$delta]:''
      );      
    }
    else{
      $form['preset_colors']['colors'][$delta] = array(
        '#type' => 'textfield',
        '#title' => t('Color ' . ($delta+1)),
        '#field_prefix' => '#',
        '#size' => 6,
        '#maxlength' => 6,
        '#default_value' => isset($form_state['values']['preset_colors']['colors'][$delta])?$form_state['values']['preset_colors']['colors'][$delta]:''
      );
    }
  }
  
  $form['preset_colors']['tagmap_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add new field'),
    '#submit' => array('tagmap_add_color_submit'), // If no javascript action.
    '#ahah' => array(
      'path'    => 'tagmap/preset_js',
      'wrapper' => 'tagmap-colors',
      'method' => 'replace',
      'effect' => 'fade'
    ),    
  );
  
  $form['tagmap_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save color preset'),
  );  
  
  return $form;
}

/**
 * A handler for adding a new color field to the preset form
 * @param $form
 * @param &$form_state
 */
function tagmap_add_color_submit($form, &$form_state) {
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['count'] = count($form_state['values']['preset_colors']['colors']) +1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Validate the new preset
 * @param &$form_state
 */
function tagmap_preset_form_validate($form, &$form_state) {
  $error = FALSE;
  if ($form_state['clicked_button']['#id'] == 'edit-tagmap-save') {
    $empty = TRUE;
    $name = trim($form_state['values']['name']);
    if (empty($name)) {
      form_set_error('name', t('The preset name is required.'));
      $error = TRUE;
    }
    for ( $delta = 0; $delta < count($form_state['values']['preset_colors']['colors']); $delta++) {
      if (module_exists('colorpicker')) {
        if (!empty($form_state['values']['preset_colors']['colors'][$delta])) {
          $empty = FALSE;
          break;
        }
      }
      else {
        $color_content = trim($form_state['values']['preset_colors']['colors'][$delta]);
        if (!empty($color_content)) {
          $empty = FALSE;
          $color_content = '#' . $color_content;
          if (!preg_match('/^#(([a-fA-F0-9]{3}$)|([a-fA-F0-9]{6}$))/', $color_content)) {
            form_set_error('preset_colors][colors][' . $delta, t('Not a valid color code.'));
            $error = TRUE;
          }
          break;
        }        
      }      
    }
    if ($empty) {
      $error = TRUE;
      form_set_error('preset_colors', t('At least one color must be entered.'));
    }
    if ($error)
      drupal_add_css(drupal_get_path('module', 'tagmap') . '/tagmap.css');
  }
}

/**
 * Create a new preset submit handler
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-tagmap-save') {
    $colors = array();
    for ( $delta = 0; $delta < count($form_state['values']['preset_colors']['colors']); $delta++) {
      $color = trim($form_state['values']['preset_colors']['colors'][$delta]);
      if (!empty($color)) {
        if (!module_exists('colorpicker'))
          $color = '#' . $color;
        $colors[] = $color;
      }
    }
    $preset = new stdClass();
    $preset->preset_name = $form_state['values']['name'];
    if (drupal_write_record('tagmap_preset', $preset)) {
      foreach ($colors as $key => $value) {
        $color = new stdClass();
        $color->prid = $preset->prid;
        $color->color_code = $value;
        $color->color_index = $key;
        if (!drupal_write_record('tagmap_preset_color', $color)) {
          drupal_set_message(t('Error writing the color with the code: @code!', array('@code' => $value)), 'error');
          db_query('DELETE FROM {tagmap_preset_color} WHERE prid=%d', $preset->prid);
          return;
        }
      }
    }
    else{
      drupal_set_message(t('The preset failed to save!'), 'error');
      return;;
    }
    drupal_set_message(t('The color preset was saved successfully.'), 'status');
    unset($form_state['values']);
  } 
}

/**
 * CEdit an existing preset form
 * @param $form_state
 */
function tagmap_preset_edit_form($form_state) {
  if (!module_exists('colorpicker') && $_GET['q'] == 'admin/settings/tagmap/presets/new_preset')
    drupal_set_message(t('For additional functionality install Colorpicker module from !link', array('!link' => l('here', 'http://drupal.org/project/colorpicker', array('attributes' => array('target' => '_blank'))))), 'warning');
  if (!isset($form_state['values'])) {
    $preset = db_fetch_object(db_query("SELECT prid, preset_name FROM {tagmap_preset} WHERE prid=%d", arg(5)));
    $form_state['values']['name'] = $preset->preset_name;
    $form_state['values']['prid'] = $preset->prid;
    $results = db_query("SELECT colorid, color_code, color_index FROM {tagmap_preset_color} WHERE prid=%d ORDER BY color_index", arg(5));
    $counter = 0;
    while ($result = db_fetch_object($results)) {
      $form_state['values']['preset_colors']['colors'][$result->color_index]['color'] = module_exists('colorpicker') ? $result->color_code : substr($result->color_code, 1);
      $form_state['values']['preset_colors']['colors'][$result->color_index]['id'] = $result->colorid;
      $counter++;
    }
    $form_state['count'] = $counter;
  }
  //If the form fail validation, remember the #action attribute
  //so it doesn't redirect to the ahah callback
  if (!isset($form_state['storage']['old_action'])) {
    $form_state['storage']['old_action'] = isset($form['#action']) ? $form['#action'] : request_uri();
  }
  // If the form action change, restore the old one.
  if (isset($form_state['storage']['old_action']) && ($form['#action']!=$form_state['storage']['old_action'])) {
    $form['#action'] = $form_state['storage']['old_action'];
  }   

  drupal_add_css(drupal_get_path('module', 'tagmap') . '/tagmap.css');
  
  $form['prid'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['prid']
  );
  
  $form['#redirect'] = 'admin/settings/tagmap/presets/presetlist';
  
  $form['name_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preset name'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );  
  $form['name_fieldset']['name'] = array(
    '#type' => 'textfield',
//    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['name'])?$form_state['values']['name']:''
  );
  
  $form['preset_colors'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#attributes' => array('class' => 'ahah-wrapper'),
    '#title' => t('Color codes'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['preset_colors']['colors'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="tagmap-colors">',
    '#suffix' => '</div>',
  );
  
  $color_count =  isset($form_state['count']) ? $form_state['count'] : 1;
  for ($delta = 0; $delta < $color_count; $delta++) {
    if (module_exists('colorpicker')) {
      $form['preset_colors']['colors'][$delta]['color'] = array(
        '#type' => 'colorpicker_textfield',
        '#field_prefix' => '#',
        '#title' => t('Color ' . ($delta+1)),
        '#default_value' => isset($form_state['values']['preset_colors']['colors'][$delta]['color'])?$form_state['values']['preset_colors']['colors'][$delta]['color']:''
      );      
    }
    else {
      $form['preset_colors']['colors'][$delta]['color'] = array(
        '#type' => 'textfield',
        '#title' => t('Color ' . ($delta+1)),
        '#field_prefix' => '#',
        '#size' => 6,
        '#maxlength' => 6,
        '#default_value' => isset($form_state['values']['preset_colors']['colors'][$delta]['color'])?$form_state['values']['preset_colors']['colors'][$delta]['color']:''
      );
    }
    $form['preset_colors']['colors'][$delta]['id'] = array(
      '#type' => 'hidden',
      '#value' => isset($form_state['values']['preset_colors']['colors'][$delta]['id'])?$form_state['values']['preset_colors']['colors'][$delta]['id']:''
    );     
  }
  
  $form['preset_colors']['tagmap_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add new field'),
    '#submit' => array('tagmap_add_color_submit'), // If no javascript action.
    '#ahah' => array(
      'path'    => 'tagmap/preset_js',
      'wrapper' => 'tagmap-colors',
      'method' => 'replace',
      'effect' => 'fade'
    ),    
  );
  
  $form['tagmap_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save color preset'),
  );
  $form['tagmap_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );     
  
  return $form;  
}

/**
 * Validate the editid preset
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_edit_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-tagmap-cancel')
    return;
  $error = FALSE;
  if ($form_state['clicked_button']['#id'] == 'edit-tagmap-save') {
    $empty = TRUE;
    $name = trim($form_state['values']['name']);
    if (empty($name)) {
      form_set_error('name', t('The preset name is required.'));
      $error = TRUE;
    }
    for ( $delta = 0; $delta < (count($form_state['values']['preset_colors']['colors'])); $delta++) {
      if (module_exists('colorpicker')) {
        if (!empty($form_state['values']['preset_colors']['colors'][$delta]['color'])) {
          $empty = FALSE;
          break;
        }
      }
      else {
        $color_content = trim($form_state['values']['preset_colors']['colors'][$delta]['color']);
        if (!empty($color_content)) {
          $empty = FALSE;
          $color_content = '#' . $color_content;
          if (!preg_match('/^#(([a-fA-F0-9]{3}$)|([a-fA-F0-9]{6}$))/', $color_content)) {
            form_set_error('preset_colors][colors][' . $delta . '][color', t('Not a valid color code.'));
            $error = TRUE;
          }
          break;
        }        
      }      
    }
    if ($empty) {
      $error = TRUE;
      form_set_error('preset_colors', t('At least one color must be entered.'));
    }
    if ($error)
      drupal_add_css(drupal_get_path('module', 'tagmap') . '/tagmap.css');
  }
}

/**
 * Submit the changed preset
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_edit_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-tagmap-cancel') {
    drupal_goto('admin/settings/tagmap/presets/presetlist');
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-tagmap-save') {
    unset($form_state['storage']);
    $colors = array();
    $ofset = 0;
    for ( $delta = 0; $delta < count($form_state['values']['preset_colors']['colors']); $delta++) {
      $color = trim($form_state['values']['preset_colors']['colors'][$delta]['color']);
      if (!empty($color)) {
        if (!module_exists('colorpicker'))
          $color = '#' . $color;
        $colors[$delta - $ofset]['color'] = $color;
        $colors[$delta - $ofset]['id'] = $form_state['values']['preset_colors']['colors'][$delta]['id'];
      }
      elseif (!empty($form_state['values']['preset_colors']['colors'][$delta]['id'])) {
        if (db_query('DELETE FROM {tagmap_preset_color} WHERE colorid = %d',  $form_state['values']['preset_colors']['colors'][$delta]['id'])) {
          $ofset++;
        }
      }
    }
    $preset = new stdClass();
    $preset->preset_name = $form_state['values']['name'];
    $preset->prid = $form_state['values']['prid'];
    if (drupal_write_record('tagmap_preset', $preset, 'prid')) {
      foreach ($colors as $key => $value) {
        $color = new stdClass();
        $color->prid = $preset->prid;
        $color->color_code = $value['color'];
        $color->color_index = $key;
        if (!empty($value['id'])) {
          $color->colorid = $value['id'];
          $success = drupal_write_record('tagmap_preset_color', $color, 'colorid');
        }
        else
          $success = drupal_write_record('tagmap_preset_color', $color);
        if (!$success) {
          drupal_set_message(t('Error writing the color with the code: @code!', array('@code' => $value)), 'error');
          return;
        }
      }
    }
    else {
      drupal_set_message(t('The preset failed to update!'), 'error');
      return;;
    }
    drupal_set_message(t('The color preset was updated successfully.'), 'status');
    unset($form_state['values']);
  }   
}

/**
 * Export the preset from the database
 * @param $form_state
 */
function tagmap_preset_export_form($form_state) {
  $preset_name = db_result(db_query("SELECT preset_name FROM {tagmap_preset} WHERE prid = %d", arg(5)));
  $results = db_query("SELECT color_index, color_code FROM {tagmap_preset_color} WHERE prid = %d ORDER BY color_index", arg(5));
  $colors = '$colors = array(' . "\n";
  $lines=0;
  while ($result = db_fetch_object($results)) {
    $colors .= "  " . $result->color_index . " => '" . $result->color_code . "',\n"; $lines++;
  }
  $colors .= ");"; $lines +=2;
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Color preset name'),
    '#default_value' => $preset_name,
  ); 
    
  $form['export'] = array(
    '#type' => 'textarea',
    '#default_value' => $colors,
    '#rows' => $lines,
  );
  $form['tagmap_save'] = array(
    '#value' => l('Return', 'admin/settings/tagmap/presets/presetlist'),
  );  
  return $form;
}

function tagmap_preset_import_form($form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Color preset name'),
    '#required' => TRUE,
    '#description' => t('Enter the name of the color preset here.'),
  );  
  $form['colors'] = array(
    '#type' => 'textarea',
    '#title' => t('Paste color array here.'),
    '#description' => t('It must be an array in the format: <strong>$coloes</strong> = array( 0 => \'#< max. 6 digit hex number >\', ...);'),
    '#required' => TRUE,
  );  
  $form['tagmap_import'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );  
  return $form;
}

/**
 * Validate the import string of the preset
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_import_form_validate($form, &$form_state) {
  $colors = NULL;
  @eval($form_state['values']['colors']);
  if (!isset($colors) || !is_array($colors)) {
    form_set_error('colors', t('The import data is not valid import text.'));
    return;
  } 
  foreach ($colors as $key => $value) {
    if (!is_numeric($key)) {
      form_set_error('colors', t('The key !key is not numeric.', array('!key' => $key)));
      return;
    }
    if (!preg_match('/^#(([a-fA-F0-9]{3}$)|([a-fA-F0-9]{6}$))/', $value)) {
      form_set_error('colors', t('Value !val is not a valid color code.', array('!val' => $value)));
      return;
    }    
  } 
}

/**
 * Validate the import string of the preset
 * @param $form
 * @param &$form_state
 */
function tagmap_preset_import_form_submit($form, &$form_state) {
  $colors = NULL;
  @eval($form_state['values']['colors']);

  $preset = new stdClass();
  $preset->preset_name = $form_state['values']['name'];
  if (drupal_write_record('tagmap_preset', $preset)) {
    foreach ($colors as $key => $value) {
      $color = new stdClass();
      $color->prid = $preset->prid;
      $color->color_code = $value;
      $color->color_index = $key;
      if (!drupal_write_record('tagmap_preset_color', $color)) {
        drupal_set_message(t('Error writing the color with the code: @code!', array('@code' => $value)), 'error');
        db_query('DELETE FROM {tagmap_preset_color} WHERE prid=%d', $preset->prid);
        db_query('DELETE FROM {tagmap_preset} WHERE prid=%d', $preset->prid);
        return;
      }
    }
  }
  else{
    drupal_set_message(t('The preset failed to save!'), 'error');
    return;;
  }
  drupal_set_message(t('The color preset was saved successfully.'), 'status');
  drupal_goto('admin/settings/tagmap/presets/presetlist');
}